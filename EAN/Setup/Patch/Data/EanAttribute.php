<?php

namespace Ecentura\EAN\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Eav\Setup\EavSetupFactory;
use Ecentura\EAN\Ui\DataProvider\Product\Form\Modifier\DynamicRowAttribute;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Catalog\Model\Product;

class EanAttribute implements DataPatchInterface
{

    public $moduleDataSetup;
    public $categorySetupFactory;
    public $eavSetupFactory;
    /**
     * Dependency Initilization
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param CategorySetupFactory $categorySetupFactory
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        CategorySetupFactory $categorySetupFactory,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->categorySetupFactory = $categorySetupFactory;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @inheritdoc
     */
    public function apply()
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $eavSetup->addAttribute(
            Product::ENTITY,
            DynamicRowAttribute::PRODUCT_ATTRIBUTE_CODE,
            [
                'label' => 'EAN 1',
                'type' => 'text',
                'default'  => '',
                'input' => 'text',
                'default' => '',
                'group' => 'General',
                'sort_order' => 8,
                'global' => ScopedAttributeInterface::SCOPE_WEBSITE,
                'visible' => true,
                'user_defined' => true,
                'required' => false,
                'used_in_product_listing' => true,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false,
                'visible_on_front' => false,
                'visible' => true
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getAliases()
    {
        return [];
    }
}
