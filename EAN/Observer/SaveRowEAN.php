<?php

namespace Ecentura\EAN\Observer;

use Ecentura\EAN\Ui\DataProvider\Product\Form\Modifier\DynamicRowAttribute;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Serialize\SerializerInterface;
class SaveRowEAN implements ObserverInterface
{
    /**
     * @var SerializerInterface
     */
    public $serializer;
    /**
     * @var RequestInterface
     */
    public $request;

    /**
     * Dependency Initilization
     *
     * @param RequestInterface $request
     * @param \Magento\Framework\Serialize\SerializerInterface $serializer
     */
    public function __construct(
        RequestInterface $request,
        SerializerInterface $serializer,
    ) {
        $this->request = $request;
        $this->serializer = $serializer;
    }

    /**
     * Execute
     *
     * @param Observer $observer
     * @return this
     */
    public function execute(Observer $observer)
    {
        /** @var $product \Magento\Catalog\Model\Product */
        $product = $observer->getEvent()->getDataObject();
        $wholeRequest = $this->request->getPost();
        $productData = $wholeRequest['product'];

        if(!empty($productData)) {
            $highlights = isset($productData[DynamicRowAttribute::PRODUCT_ATTRIBUTE_CODE])
                ? $productData[DynamicRowAttribute::PRODUCT_ATTRIBUTE_CODE] : '';
            $requiredParams = ['consists_of_ean', 'consists_of_number_ean'];
            if(!empty($highlights)) {
                $highlights = $this->removeEmptyArray($highlights, $requiredParams);
            }
            $product->setDynamicRowAttribute($this->serializer->serialize($highlights));
        }
    }

    /**
     * Function to remove empty array from the multi dimensional array
     *
     * @param array $attractionData
     * @param array $requiredParams
     * @return array
     */
    private function removeEmptyArray($attractionData, $requiredParams)
    {
        $requiredParams = array_combine($requiredParams, $requiredParams);
        $reqCount = count($requiredParams);

        foreach ($attractionData as $key => $values) {
            $values = array_filter($values);
            $intersectCount = count(array_intersect_key($values, $requiredParams));
            if ($reqCount !== $intersectCount) {
                unset($attractionData[$key]);
            }
        }
        return $attractionData;
    }
}
